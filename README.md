# elliptic_certbot

Description:
Elliptic certbot allows you to easily use elliptic curve algorithms with 
certbot. It was created following the instructions from here:
    https://www.ericlight.com/using-ecdsa-certificates-with-lets-encrypt

Why:
Security should be less hard than not being secure, or at the very least, easy.

Usage:
    elliptic_certbot.py [-h] [-d DOMAIN] [-w WEBROOT] [-e EMAIL] action

action:
    renew
    newcert

    newcert:
        Issue a new certificate. You must provide the domain, webroot and email
        address.
    
    renew:
        Suitable for a cron job. Checks to see if certificates is expiring (all
        that have been issued using this tool) within the day. If they are, it
        automatically renews it.
        It is recommended that this is run from cron every 12 hours or so.
        Also supports -d i.e. only attempt a renew on that one domain. It does
        not support a force function.

files:
    By default it stores a directory structure in:
        /etc/letsencrypt/elliptic
    Find current certificates in /etc/letsencrypt/elliptic/live/[domain name]