#!/usr/bin/env python

import os
import subprocess
from argparse import ArgumentParser
import sys
from ConfigParser import SafeConfigParser
import re
from datetime import datetime, timedelta
from OpenSSL import crypto

configuration_dir = '/etc/letsencrypt/elliptic'
stored_settings_dir = configuration_dir + '/renewal'
archive_dir = configuration_dir + '/archive'
live_dir = configuration_dir + '/live'
key_dir = configuration_dir + '/keys'

exitcodes = { 
    'failed dependencies': 1,
    'wrong permissions': 2,
    'nothing to do': 3,
    'invalid parameters newcert': 4,
    'no configuration file found': 5,
    'fatal - configuration structure': 6,
    'certificate exists?': 7
}

def direxists( path ):
    if os.path.exists(path) and not os.path.isdir(path):
        sys.stderr.write( path + ' exists but is not a folder. Change ' +
                'configuration of folders or remove offending file.\n')
        sys.exit( exitcodes['fatal - configuration structure'] )
    else:
        return os.path.exists(path) and os.path.isdir(path)

def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def genLiveLinks( domain ):
    liveDir = os.path.join( live_dir, domain )
    if not direxists( liveDir ):
        os.makedirs( liveDir )
    certFile = os.path.join( liveDir, 'cert.pem' )
    if os.path.exists( certFile ):
        os.unlink( certFile )
    chainFile = os.path.join( liveDir, 'chain.pem' )
    if os.path.exists( chainFile ):
        os.unlink( chainFile )
    fullchainFile = os.path.join( liveDir, 'fullchain.pem' )
    if os.path.exists( fullchainFile ):
        os.unlink( chainFile )
    privkeyFile = os.path.join( liveDir, 'privkey.key' )
    if os.path.exists( privkeyFile ):
        os.unlink( privkeyFile )

    certPattern = re.compile('cert[0-9]*.pem$')
    chainPattern = re.compile('chain[0-9]*.pem$')
    fullchainPattern = re.compile('fullchain[0-9]*.pem$')
    privkeyPattern = re.compile('privkey[0-9]*.key$')
    archiveDir = os.path.join( archive_dir, domain )
    archiveFiles = os.listdir( archiveDir )
    certFileNumbers = []
    chainFileNumbers = []
    fullchainFileNumbers = []
    privkeyFileNumbers = []
    for file in archiveFiles:
        if( certPattern.match( file )):
            certFileNumbers.append( file.replace( 'cert', 
                    '' ).replace( '.pem', '' ))
        elif( chainPattern.match( file )):
            chainFileNumbers.append( file.replace( 'chain', 
                    '' ).replace( '.pem', '' ))
        elif( fullchainPattern.match( file )):
            fullchainFileNumbers.append( file.replace( 'fullchain', 
                    '' ).replace( '.pem', '' ))
        elif( privkeyPattern.match( file )):
            privkeyFileNumbers.append( file.replace( 'privkey', 
                    '' ).replace( '.key', '' ))
    os.symlink( os.path.join(archiveDir, 'cert' + max(certFileNumbers) + 
            '.pem'), os.path.join(liveDir, 'cert.pem'))
    os.symlink( os.path.join(archiveDir, 'chain' + max(chainFileNumbers) + 
            '.pem'), os.path.join(liveDir, 'chain.pem'))
    os.symlink( os.path.join(archiveDir, 'fullchain' + 
            max(fullchainFileNumbers) + '.pem'), os.path.join(liveDir,
            'fullchain.pem'))
    os.symlink(os.path.join(archiveDir, 'privkey' + 
            max(privkeyFileNumbersFileNumbers) + '.key'), os.path.join(liveDir,
            'privkey.key'))

def renew( domain ):
    if not domain:
        configfiles = os.listdir( stored_settings_dir )
        for configfile in configfiles:
            configPath = os.path.join( stored_settings_dir, configfile )
            if os.path.isfile( configPath ):
                dorenew( configPath )
    elif os.path.isfile( os.path.join( store_settings_dir, domain + '.cfg' )):
        dorenew(os.path.join( store_settings_dir, domain + '.cfg' ))
    else:
        sys.stderr.write('Configuration file for domain ' + domain + ' not found' )
        sys.exit(exitcodes['no configuration file found'])

def dorenew( configFile ):
    parser = SafeConfigParser()
    parser.read( configFile )
    # check to see if certificate needs renewing
    domainname = parser.get('reg_details','domain')
    certFile = open( os.path.join(
            live_dir, domainname ,'cert.pem'))
    cert = crypto.load_certificate( crypto.FILETYPE_PEM, certFile.read())
    certFile.close()
    if ( datetime.today() - timedelta( days = 1 )) < (datetime.strptime( 
            cert.get_notAfter(), "%Y%m%d%H%M%SZ")):
        print 'Nothing to do for ' + domainname + '.'
    else:
        certPattern = re.compile('cert[0-9]*.pem$')
        chainPattern = re.compile('chain[0-9]*.pem$')
        fullchainPattern = re.compile('fullchain[0-9]*.pem*')
        archiveDir = os.path.join( archive_dir, domain )
        archiveFiles = os.listdir( archiveDir )
        certFileNumbers = []
        chainFileNumbers = []
        fullchainFileNumbers = []
        for file in archiveFiles:
            if( certPattern.match( file )):
                certFileNumbers.append( file.replace( 'cert', 
                        '' ).replace( '.pem', '' ))
            elif( chainPattern.match( file )):
                chainFileNumbers.append( file.replace( 'chain', 
                        '' ).replace( '.pem', '' ))
            elif( fullchainPattern.match( file )):
                fullchainFileNumbers.append( file.replace( 'fullchain', 
                        '' ).replace( '.pem', '' ))
        commandLine = ["certbot","certonly","-w","--agree-tos",
                "--non-interactive","--webroot"]
        commandLine.append( parser.get( 'reg_details','webroot' ))
        commandLine.append( "-d",domainname )
        commandLine.append( "--email", parser.get( 'reg_details','email' ))
        commandLine.append("--csr", os.path.join(archive_dir, domainname, 
                ec1.csr))
        commandLine.append("--cert-path",os.path.join(archive_dir, domain, cert 
                + max(certFileNubers)+1 + ".pem"))
        commandLine.append("--fullchain-path",os.join( archive_dir, domain, 
                "fullchain"+ max(fullchainFileNumbers)+1 +'.pem'))
        subprocess.call(commandLine)
        genLiveLinks( domain )

def newcert( email, webroot, domain ):
    # Make sure certificate doesn't alredy exist. This isn't a good test
    if os.path.isdir( os.path.join( live_dir, domain )):
        sys.stderr.write( 'Cerificate already exists?' )
        sys.exit(exitcodes['certificate exists?'])
    else:
        # Issue certificate
        working_dir = os.path.join( archive_dir, domain )
        if not direxists( working_dir ):
            os.makedirs( working_dir )
        os.chdir( working_dir )
        ps = subprocess.Popen(["openssl","ecparam","-genkey","-name",
                "secp384r1"], stdout=subprocess.PIPE)
        subprocess.call(["openssl","ec","-out","privkey1.key"], stdin=ps.stdout)
        ps.wait()
        subprocess.call(["openssl","req","-new","-sha256","-key","privkey1.key",
                "-nodes","-out","ec1.csr","-outform","pem","-batch",
                "-subj","/O="+domain+"/emailAddress="+email+"/CN="+domain])
        subprocess.call(['certbot','certonly','-w',webroot,'-d',domain,
                '--email',email,'--csr','./ec1.csr','--agree-tos','-q',
                '--webroot'])
        # Sanitize fie names
        os.rename( '0000_cert.pem', 'cert1.pem')
        os.rename( '0000_chain.pem', 'chain1.pem')
        os.rename( '0001_chain.pem', 'fullchain1.pem')
        genLiveLinks( domain )
        # Create configuration file
        os.chdir( stored_settings_dir )
        parser = SafeConfigParser()
        parser.add_section('reg_details')
        parser.set('reg_details', 'domain', domain)
        parser.set('reg_details', 'email', email)
        parser.set('reg_details', 'webroot', webroot)
        with open( domain + '.cfg', 'w' ) as configfile:
            parser.write( configfile )

def buildDirectoryStructure():
    if not direxists( configuration_dir ):
        os.makedirs( configuration_dir )
    if not direxists( stored_settings_dir ):
        os.makedirs( stored_settings_dir )
    if not direxists( archive_dir ):
        os.makedirs( archive_dir )
    if not direxists( live_dir ):
        os.makedirs( live_dir )

def verifynewcertparams( email, webroot, domain ):
    returnvalue = True
    emailpattern = re.compile( '^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$' )
    domainnamepattern = re.compile( '^([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$' )
    if not emailpattern.match( email ):
        sys.stderr.write("Invalid email address\n")
        returnvalue = False
    elif not os.path.isdir( webroot ):
        sys.stderr.write("Webroot does not exist\n")
        returnvalue = False
    elif not domainnamepattern.match( domain ):
        sys.stderr.write("Invalid domain\n")
        returnvalue = False
    return returnvalue

# Make sure we have the dependencies
#  python certbot plugin is experimental and extremely buggy at best.
#  openssl - we're just being lazy. I'm sure there are python bindings....
dependencies = ['certbot','openssl']
failed_dependencies = []
for dependency in dependencies:
        if not which( dependency ):
            failed_dependencies.append( dependency )

if failed_dependencies:
    sys.stderr.write('Not all dependencies met. Missing: \n')
    for dependency in failed_dependencies:
        sys.stderr.write('* ' + dependency + '\n' )
    sys.exit(exitcodes['failed dependencies'])

#Make sure we're running as root
if os.geteuid() != 0:
    sys.stderr.write('This program must be run as root.\n')
    sys.ext(exitcodes['wrong permissions'])

#argument parsing
parser = ArgumentParser(description='Use elliptic curve cryptography with ' +
        'let\'s encrypt.\nUsing instructions from Eric Light: ' + 
        'https://www.ericlight.com/using-ecdsa-certificates-with-lets-encrypt',
        epilog='For further security, don\'t forget to look into' +
        'Diffie-Hellman key exchange.')
parser.add_argument( "action", help="newcert OR renew" )
parser.add_argument( "-d", "--domain", type=str, 
        help="Domain name. Mandatory with newcert, optional with renew.")
parser.add_argument( "-w", "--webroot", type=str, help="Webroot." )
parser.add_argument( "-e", "--email", type=str, help="Registration email address" )
args = parser.parse_args()
if args.action != 'renew' and args.action != 'newcert':
    sys.stderr.write( 'Invalid action. Must be either renew or newcert.\n\n');
    parser.print_help()

if args.action == 'newcert':
    # Check to see if we have all of the parameters that we need
    if not args.email or not args.webroot or not args.domain:
        sys.stderr.write( "Necessary arguments not provided. Usage:\n " +
                os.path.basename( __file__ ) + " newcert -d domainname -w " +
                "webroot -e emailaddress\n\n" )
        sys.exit(exitcodes['invalid parameters newcert'])
    else:
        email = args.email
        webroot = args.webroot
        domain = args.domain
        if not verifynewcertparams( email, webroot, domain ):
            sys.exit(exitcodes['invalid parameters newcert'])
        buildDirectoryStructure()
        newcert( email, webroot, domain )
elif args.action == 'renew':
    if not os.path.isdir( stored_settings_dir ):
        sys.stderr.write('Configuration directory does not exist. Nothing to do.\n')
        sys.exit(exitcodes['nothing to do'])
    renew( args.domain )